Der Placer ist ein Bestückungskopf, der für die genaue Ausrichtung sowie Platzierung von Komponenten konzipiert wurde. Der Kopf kann sowohl mit Standard- als auch mit prozessspezifischen Werkzeugen ausgestattet werden. Das Spektrum reicht von vakuumbasierten Werkzeugen für Bauteilbestückung bis hin zu Stempeln für Pin-Transfer-Prozesse. Daneben garantiert ein Z-Achs-Hub von 150 mm eine außergewöhnliche Reichweite. So lassen sich auch Positionen in tiefen Kavitäten problemlos erreichen.

<!-- 2021 (C) Häcker Automation GmbH -->
