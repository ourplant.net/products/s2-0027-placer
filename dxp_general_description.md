The Camera 3D is an inspection head with 3D optical detection and automatic position correction. Using a stereoscopic camera system, it observes the component and substrate surfaces from two different perspectives. From the acquired image data, an exact 3D position is calculated.

Error compensation algorithms recognise deviations from target positions and correct them by reorienting the processing heads. By means of a Z-axis stroke positions at very different heights can be detected.

<!-- 2021 (C) Häcker Automation GmbH -->